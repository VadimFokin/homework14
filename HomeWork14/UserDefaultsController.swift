import UIKit

class UserDefaultsController: UIViewController {

    @IBOutlet weak var userLabel: UILabel!
    
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var surnameField: UITextField!

    @IBAction func nameField(_ sender: Any) {
        Persistance.shared.userName = nameField.text }
    
    @IBAction func surnameField(_ sender: Any) {
        Persistance.shared.userSurname = surnameField.text }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userLabel.text = "  "
        
        if Persistance.shared.userName != nil { userLabel.text! += Persistance.shared.userName! + " "
            nameField.placeholder = Persistance.shared.userName }
        if Persistance.shared.userSurname != nil { userLabel.text! += Persistance.shared.userSurname!
            surnameField.placeholder = Persistance.shared.userSurname }

    }
}

