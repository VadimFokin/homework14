//
//  Persistance.swift
//  HomeWork14
//
//  Created by Вадим on 27.05.2020.
//  Copyright © 2020 Vadim Fokin. All rights reserved.
//

import Foundation

class Persistance {
    static let shared = Persistance()
    
    private let kUserNameKey = "Persistance.kUserNameKey"
    
    private let kUserSurnameKey = "Persistance.kUserSurnameKey"
    
    var userName: String? {
        set { UserDefaults.standard.set(newValue, forKey: kUserNameKey) }
        get { return UserDefaults.standard.string(forKey: kUserNameKey) }
    }
    
    var userSurname: String? {
        set{ UserDefaults.standard.set(newValue, forKey: kUserSurnameKey) }
        get{ return UserDefaults.standard.string(forKey: kUserSurnameKey) }
    }
}
